var express = require('express'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    port = process.env.PORT || 3000,
    router = express.Router();

var app = express();

app.use(bodyParser());
mongoose.connect('mongodb://localhost/technologies');
var technologySchema = mongoose.Schema({
    name: String,
});

var Technology = mongoose.model('Technology', technologySchema);

router.route('/technologies')
    .get(function (req, res) {
        Technology.find(function (err, technologies) {
            if (err) {
                res.send(err);
            }
            res.send(technologies);
        });
    })
   /* .post(function (req, res) {
        var technology = new Technology();
        technology.name = req.body.name;

        technology.save(function (err) {
            if (err) {
                res.send(err);
            }
            res.send({message: "LOL created"});
        });
    })*/

router.route('/newtechnology')
    .post(function (req, res) {
        var technology = new Technology();
        technology.name = req.body.name;

        technology.save(function (err) {
            if (err) {
                res.send(err);
            }
            res.send({message: "LOL created"});
        });
    })
//app.use(express.static('public'));
app.use(router);
app.use(express.static('public'));
app.listen(3000, function () {
    console.log('Express server is up on port 3000');
});
