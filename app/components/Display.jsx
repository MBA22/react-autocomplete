var React = require('react');
var Axios = require('axios');

var Display = React.createClass({

    getInitialState: function () {
        return ({
            technologies: [],
        })
            ;
    },

    componentDidMount: function () {
        var _this = this;
        Axios.get('/technologies')
            .then(function (response) {
                _this.setState({
                    technologies: response.data,
                });
                console.log(response);
            })
            .catch(function (error) {
                _this.setState({
                    technologies: error,
                });
            });
    },

    postTechnology: function () {
        var _this = this;
        var technologyName = this.refs.inputTechnology.value;
        console.log(!this.state.technologies.filter(function (technology) {
            return technology.name == technologyName;
        }));
        if (this.state.technologies.filter(function (technology) {
                return technology.name == technologyName;
            }) == false) {
            Axios.post('/newtechnology', {name: technologyName})
                .then(function (response) {
                    console.log('saved successfully');
                    Axios.get('/technologies')
                        .then(function (response) {
                            _this.setState({
                                technologies: response.data,
                            });
                            console.log(response);
                        })
                        .catch(function (error) {
                            _this.setState({
                                technologies: error,
                            });
                        });


                });
        }
    },

    render: function () {
        return (
            <div className="col-xs-12 col-md-6">
                <div className="form-group">
                    <input className="form-control" ref="inputTechnology" list="technologies" type="text"/>
                </div>
                <datalist id="technologies">
                    {
                        this.state.technologies.map(function (item, i) {
                            return (<option key={i} value={item.name}></option>);
                        })
                    }
                </datalist>

                <button className="btn btn-success" onClick={this.postTechnology}>Save</button>
                <ul className="list-group">
                    {
                        this.state.technologies.map(function (item, i) {
                            return (<li className="list-group-item" key={i}>{item.name}</li>);
                        })
                    }
                </ul>
            </div>
        );
    },
});

module.exports = Display;