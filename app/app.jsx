var React = require('react');
var ReactDOM = require('react-dom');
var Display = require('Display');

ReactDOM.render(
    <div className="container-fluid">
        <br/>
        <Display></Display>
    </div>,
    document.getElementById('app')
);
